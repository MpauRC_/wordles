const fs = require('fs');
const path = require('path');

const direccion = path.join(__dirname, "palabras.json");

const CargarPalabras = async () => {
    if (fs.existsSync(direccion)) {
        let archivo = fs.readFileSync(direccion);
        let datos = JSON.parse(archivo);
        return datos;
    }
};

module.exports = {
    CargarPalabras,
};