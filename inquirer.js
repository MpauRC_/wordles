const { menu } = require("./input");

const menu = async () => {
    const opciones = [
        // {
        //     name: "palabras",
        //     type: "input",
        //     message: "Seleccione la opción deseada: ",
        // },
        { 
            name: "opcion",
            type: "list",
            message: "Seleccione la opción",
            choices: [
                { value: 1, name: "Jugar"},
                { value: 2, name: "No jugar"},
                { value: 3, name: "Salir"},
            ],
        },
    ];
    return inquirer.prompt(opciones);
};

async function main() {
    let opcion = await menu();
    console.log(opcion);
    console.log(opcion.palabras);
}
main();