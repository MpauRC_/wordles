const { menu, menu2 } = require("./input");

async function main() {
    let opcion = { opcion: 0 };
    while (opcion.opcion != 3){
        opcion = await menu();
        switch (opcion.opcion){
            case 1:
                let x = { opcion: 0 };
                x = await menu2();
                console.log(x.opcion);
                console.log("Prueba de crear cuenta");
                break;
            case 2:
                console.log("Prueba Iniciar sesión");
                break;
            case 3:
                console.log("Prueba de salir del menu");
                break;
        }
    }
}

main();

// const menu = async () => {
//     const opciones = [
//         { 
//             name: "sesion",
//             type: "list",
//             message: "Seleccione la opción deseada: ",
//             choices: [
//                 { value: 1, name: "Crea una cuenta" },
//                 { value: 2, name: "Inicia sesión en tu cuenta" },
//                 { value: 3, name: "Salir" },

//             ],
//         },
//         {
//             name: "opcion",
//             type: "list",
//             message: "Seleccione la opción indicada: ",
//             choices: [
//                 { value: 1, name: "Crea un nuevo juego" },
//                 { value: 2, name: "Consulta tus estadísticas" },
//                 { value: 3, name: "Sal del juego ahora" },
//             ],
//         },
//     ];
//     return inquirer.prompt(opciones);
// };

// async function main() {
//     let opcion = await menu();
//     console.log(opcion);
//     console.log(opcion.sesion);

// }

// main();